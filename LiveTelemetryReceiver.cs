﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LiveTelemetry
{
    class LiveTelemetryReceiver
    {
        private LiveTelemetry Main;

        private HashSet<string> RegisteredKeys;

        internal LiveTelemetryReceiver(LiveTelemetry main)
        {
            this.Main = main;
        }


        internal void initReceiver(string[] VALUE_KEYS)
        {
            RegisteredKeys = new HashSet<string>();

            foreach (var key in VALUE_KEYS)
            {
                string[] pieces = key.Split('.');
                string keyTemp = pieces[pieces.Length - 1];

                if (keyTemp.Trim() != "")
                {
                    if (keyTemp.ToLower().Contains("time"))
                    {
                        Main.PluginManager.AddProperty(keyTemp, this.GetType(), new TimeSpan().GetType());
                    }
                    else
                    {
                        Main.PluginManager.AddProperty(keyTemp, this.GetType(), "".GetType());
                    }

                    RegisteredKeys.Add(keyTemp);
                }
            }
        }


        internal async void receiveDataAsync(HttpResponseMessage message)
        {
            Stream stream = await message.Content.ReadAsStreamAsync();
            StreamReader reader = new StreamReader(stream);

            string[] keyPairs = reader.ReadToEnd().Split('&');

            

            foreach (string item in keyPairs)
            {
                try
                {
                    string edit = item;
                    edit = edit.Replace("+", " ");
                    edit = edit.Replace("%3A", ":");
                    edit = edit.Replace("%23", "#");

                    string[] pair = edit.Split('=');

                    //Late Registration
                    if (!RegisteredKeys.Contains(pair[0]) && pair[0].Trim() != "")
                    {
                        if (pair[0].ToLower().Contains("time"))
                        {
                            Main.PluginManager.AddProperty(pair[0], this.GetType(), new TimeSpan().GetType());
                        }
                        else
                        {
                            Main.PluginManager.AddProperty(pair[0], this.GetType(), "".GetType());
                        }

                        RegisteredKeys.Add(pair[0]);
                    }


                    //Writing
                    if (pair[0].ToLower().Contains("time"))
                    {
                        TimeSpan span = new TimeSpan();

                        if (pair[1].Split(':').Length == 3)
                        {
                            string[] time = pair[1].Split(':');
                            span = new TimeSpan(Int32.Parse(time[0]), Int32.Parse(time[1]), Int32.Parse(time[2]));
                        }
                        else
                        {
                            int min = Int32.Parse(pair[1].Split(':')[0]);

                            string[] time = pair[1].Split(':')[1].Split('.');

                            span = new TimeSpan(0, 0, min, Int32.Parse(time[0]), Int32.Parse(time[1]));
                        }

                        Main.PluginManager.SetPropertyValue(pair[0], this.GetType(), span);
                    }
                    else
                    {
                        Main.PluginManager.SetPropertyValue(pair[0], this.GetType(), pair[1]);
                    }
                }
                catch
                {

                }
            }

            reader.Close();
            stream.Close();
        }
    }
}
