﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace LiveTelemetry
{
    /// <summary>
    /// SettingsPanel
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public const string PROFILESFILE = "PluginsData/LiveTelemetryProfiles.xml";

        private LiveTelemetry mainPlugin;

        private List<Profile> Profiles = new List<Profile>();

        public SettingsControl(LiveTelemetry mainPlugin)
        {
            InitializeComponent();

            this.mainPlugin = mainPlugin;
            
            //Load Profiles
            try
            {
                if (File.Exists(PROFILESFILE))
                {
                    XmlSerializer serializer = new XmlSerializer(new Profile[1].GetType());
                    FileStream stream = new FileStream(PROFILESFILE, FileMode.Open, FileAccess.Read, FileShare.Read);

                    object obj = serializer.Deserialize(stream);

                    stream.Close();

                    if (obj is Profile[])
                    {
                        Profile[] arr = (Profile[])obj;
                        Profiles.AddRange(arr);
                    }
                }
            }
            catch (Exception)
            {

            }

            btnDeleteProfile.IsEnabled = false;
            update();
        }

        public void update()
        {
            cbProfileSelector.Items.Clear();

            foreach (var item in Profiles)
            {
                cbProfileSelector.Items.Add(item.Login + " - " + item.ServerAddress);
            }
            cbProfileSelector.Items.Add("None");

            if (mainPlugin.ServerAddress != null)
                tbServerAddress.Text = mainPlugin.ServerAddress;
            else
                tbServerAddress.Text = "";


            if (mainPlugin.Login != null)
                tbLogin.Text = mainPlugin.Login;
            else
                tbLogin.Text = "";


            if (mainPlugin.Password != null)
                tbPW.Text = mainPlugin.Password;
            else
                tbPW.Text = "";



            tbSendRate.Text = "" + mainPlugin.SendRate;

            chkGamePriority.IsChecked = false;
            chkSimHubLowPriority.IsChecked = false;
            chkAutoConnect.IsChecked = false;

            if (mainPlugin.Settings.gameHighPriorityMode)
                chkGamePriority.IsChecked = true;

            if (mainPlugin.Settings.simHubLowPriorityMode)
                chkSimHubLowPriority.IsChecked = true;

            if (mainPlugin.Settings.disableAutoConnect)
                chkAutoConnect.IsChecked = true;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            mainPlugin.ServerAddress = tbServerAddress.Text.Trim();
            mainPlugin.Login = tbLogin.Text.Trim();
            mainPlugin.Password = tbPW.Text.Trim();

            int temp = 0;
            if (Int32.TryParse(tbSendRate.Text.Trim(),out temp))
                mainPlugin.SendRate = temp;
            else
                mainPlugin.SendRate = 10;

            //Making sure to remove slashes at the end
            while (mainPlugin.ServerAddress.LastIndexOf('/') == (mainPlugin.ServerAddress.Length - 1) && mainPlugin.ServerAddress != "")
            {
                mainPlugin.ServerAddress = mainPlugin.ServerAddress.Substring(0, (mainPlugin.ServerAddress.Length - 1));
            }

            if (mainPlugin.ServerAddress.ToLower().Contains("http://"))
                mainPlugin.ServerAddress = mainPlugin.ServerAddress.Substring("http://".Length);
            if (mainPlugin.ServerAddress.ToLower().Contains("https://"))
                mainPlugin.ServerAddress = mainPlugin.ServerAddress.Substring("https://".Length);

            try
            {
                Uri uri = new Uri("http://" + mainPlugin.ServerAddress);
            }
            catch
            {
                mainPlugin.ServerAddress = null;
            }

            mainPlugin.Settings.gameHighPriorityMode = chkGamePriority.IsChecked.Value;
            mainPlugin.Settings.simHubLowPriorityMode = chkSimHubLowPriority.IsChecked.Value;
            mainPlugin.Settings.disableAutoConnect = chkAutoConnect.IsChecked.Value;

            update();

            mainPlugin.SaveSettings();

            mainPlugin.Mode = LiveTelemetry.TelemetryMode.None;

            if (mainPlugin.ServerAddress != null)
            {
                //Setting up HTTPClient
                mainPlugin.setupHttpClient()

                if (mainPlugin.dataSendThread == null)
                {
                    mainPlugin.startDataThread(); //Starting up stuff
                }
            }
            else if (mainPlugin.dataSendThread != null && mainPlugin.turnedOff != true)
            {
                //Shutting down the updater, as there is no server address entered
                mainPlugin.turnedOff = true;
                mainPlugin.dataSendThread.Join();

                mainPlugin.dataSendThread = null;
            }
        }

        private void btnSaveProfile_Click(object sender, RoutedEventArgs e)
        {
            Profile profile = new Profile();
            profile.ServerAddress = tbServerAddress.Text.Trim();
            profile.Login = tbLogin.Text.Trim();
            profile.Password = tbPW.Text.Trim();

            int temp = 0;
            if (Int32.TryParse(tbSendRate.Text.Trim(), out temp))
                profile.SendRate = temp;
            else
                profile.SendRate = 10;

            //Making sure to remove slashes at the end
            while (profile.ServerAddress.LastIndexOf('/') == (profile.ServerAddress.Length - 1))
            {
                profile.ServerAddress = profile.ServerAddress.Substring(0, (profile.ServerAddress.Length - 1));
            }

            if (profile.ServerAddress.ToLower().Contains("http://"))
                profile.ServerAddress = profile.ServerAddress.Substring("http://".Length);
            if (profile.ServerAddress.ToLower().Contains("https://"))
                profile.ServerAddress = profile.ServerAddress.Substring("https://".Length);

            try
            {
                Uri uri = new Uri("http://" + profile.ServerAddress);
            }
            catch
            {
                profile.ServerAddress = null;
            }

            //Adding it
            bool isContained = false;
            for (int i = 0; i < Profiles.Count; i++)
            {
                Profile item = Profiles[i];
                if (item.ServerAddress == profile.ServerAddress && item.Login == profile.Login)
                {
                    isContained = true;
                    Profiles[i] = profile;
                    break;
                }
            }

            if (!isContained)
                Profiles.Add(profile);


            saveProfiles();
            
            update();
        }

        private void saveProfiles()
        {
            //Saving
            Profile[] arr = Profiles.ToArray();
            XmlSerializer serializer = new XmlSerializer(arr.GetType());
            FileStream stream = new FileStream(PROFILESFILE, FileMode.Create, FileAccess.Write, FileShare.None);

            serializer.Serialize(stream, arr);

            stream.Close();
        }

        private void cbProfileSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = cbProfileSelector.SelectedIndex;

            if (index < Profiles.Count && index != -1)
            {
                Profile profile = Profiles[index];

                tbServerAddress.Text = profile.ServerAddress;
                tbLogin.Text = profile.Login;
                tbPW.Text = profile.Password;
                tbSendRate.Text = "" + profile.SendRate;

                btnDeleteProfile.IsEnabled = true;
            }
            else
            {
                if (mainPlugin.ServerAddress != null)
                    tbServerAddress.Text = mainPlugin.ServerAddress;
                else
                    tbServerAddress.Text = "";


                if (mainPlugin.Login != null)
                    tbLogin.Text = mainPlugin.Login;
                else
                    tbLogin.Text = "";


                if (mainPlugin.Password != null)
                    tbPW.Text = mainPlugin.Password;
                else
                    tbPW.Text = "";



                tbSendRate.Text = "" + mainPlugin.SendRate;

                btnDeleteProfile.IsEnabled = false;
            }
        }

        private void btnDeleteProfile_Click(object sender, RoutedEventArgs e)
        {
            int index = cbProfileSelector.SelectedIndex;
            if (index < Profiles.Count && index != -1)
            {
                Profile profile = Profiles[index];
                MessageBoxResult result = MessageBox.Show("The Profile "+profile.Login + " - "+profile.ServerAddress + "will be deleted and lost forever", "Are you sure?", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                if (result == MessageBoxResult.OK)
                {
                    //It is happening
                    Profiles.Remove(profile);

                    saveProfiles();

                    update();
                }
            }
        }

        [Serializable]
        public class Profile
        {
            public string Login { get; set; }
            public string Password { get; set; }
            public string ServerAddress { get; set; }
            public int SendRate { get; set; }
        }
    }
}
