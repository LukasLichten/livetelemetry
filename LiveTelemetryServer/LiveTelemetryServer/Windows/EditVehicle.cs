﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveTelemetryServer;

namespace LiveTelemetryServer.Windows
{
    public partial class EditVehicle : Form
    {
        private MainForm Main;

        private Vehicle Veh;

        public EditVehicle(MainForm main, Vehicle veh)
        {
            this.Main = main;
            this.Owner = Main;

            this.Veh = veh;

            InitializeComponent();


            tbLogin.Text = Veh.Login;
            tbPw.Text = Veh.Password;

            this.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string login = tbLogin.Text.Trim();
            string password = tbPw.Text.Trim();

            if (login == "" || password == "")
            {
                MessageBox.Show("You must enter a Login and a Password, otherwise your data can not be saved", "Incomplete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                tbLogin.Text = Veh.Login;
                tbPw.Text = Veh.Password;
                return;
            }

            

            if (login != Veh.Login && Main.Data.Values.ContainsKey(login))
            {
                MessageBox.Show("You must use a different Login", "Login already in use", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Veh.Password = password;

            if (login != Veh.Login)
            {
                //Change of Login name
                Main.Data.Values.Remove(Veh.Login);

                Veh.Login = login;

                Main.Data.Values.Add(Veh.Login, Veh);
            }

            Main.fillDatabaseList();

            this.Visible = false;
            this.Owner = null;
            this.Dispose();
        }
    }
}
