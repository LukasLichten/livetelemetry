﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveTelemetryServer;

namespace LiveTelemetryServer.Windows
{
    public partial class CreateVehicle : Form
    {
        private MainForm Main;

        public CreateVehicle(MainForm main)
        {
            this.Main = main;
            this.Owner = Main;

            InitializeComponent();

            this.Show();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string login = tbLogin.Text.Trim();
            string password = tbPw.Text.Trim();

            if (login == "" || password == "")
            {
                MessageBox.Show("You must enter a Login and a Password to be able to create an account", "Incomplete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (Main.Data.Values.ContainsKey(login))
            {
                MessageBox.Show("You must use a different Login", "Login already in use", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Vehicle veh = new Vehicle(login, password);
            Main.Data.Values.Add(login, veh);

            Main.fillDatabaseList();

            this.Visible = false;
            this.Owner = null;
            this.Dispose();
        }
    }
}
