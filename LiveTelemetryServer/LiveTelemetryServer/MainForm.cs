﻿using LiveTelemetryServer.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LiveTelemetryServer
{
    public partial class MainForm : Form
    {
        public const string SETTINGFILE = "config.xml";

        internal TelemetryServer Server { private set; get; }
        internal Database Data { private set; get; }

        public MainForm()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(main_closing);

            LoadSettings();

            Data = new Database("Database\\Acc.dat");

            fillDatabaseList();
            timerUiUpdater_Tick(null, null);
            timerUiUpdater.Enabled = true;

            if (chkAutoStart.Checked)
            {
                btnStartStop_Click(null, null);
            }
        }

        private void main_closing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;

            Data.StopAutoSave = true;

            if (Server != null)
                Server.Stop();

            SaveSettings();

            System.Threading.Thread.Sleep(1000);

            TimeSpan since = DateTime.UtcNow - Data.lastSave;

            if (since > new TimeSpan(0,0,10))
            {
                Data.Save();
            }
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if (Server == null)
            {
                //Start
                tbPort.Enabled = false;
                btnStartStop.Text = "Starting...";

                SaveSettings();

                int port = 0;
                if (!Int32.TryParse(tbPort.Text, out port))
                    port = 5128;

                tbPort.Text = "" + port;

                Server = new TelemetryServer(port, this);

                btnStartStop.Text = "Stop";
                lblStatus.Text = "Online";
                
            }
            else
            {
                //Shutdown
                tbPort.Enabled = true;
                btnStartStop.Text = "Stopping...";

                Server.Stop();
                Server = null;

                btnStartStop.Text = "Start";
                lblStatus.Text = "Offline";
            }

            
        }

        private void timerUiUpdater_Tick(object sender, EventArgs e)
        {
            if (Server != null)
            {
                //Server Online
                if (!lblRequestRate.Visible)
                    lblRequestRate.Visible = true;


                int num = Server.NumberOfRequests;
                Server.NumberOfRequests = 0;
                lblRequestRate.Text = "Requests/Sec: " + num;
            }
            else
            {
                //Server Offline
                lblRequestRate.Visible = false;
            }

            if (Data.lastSave != null && Data.lastSave.Year != 1)
            {
                if (!lblLastSave.Visible)
                    lblLastSave.Visible = true;

                TimeSpan span = DateTime.UtcNow - Data.lastSave;
                int min = (int)(span.TotalMinutes + 0.5);

                lblLastSave.Text = "Last Database Save: " + min + "min";
            }
            else
            {
                lblLastSave.Visible = false;
            }
        }

        internal void fillDatabaseList()
        {
            string[] filters = null;

            if (tbVehicleSearch.Text.Trim() != "")
            {
                string text = tbVehicleSearch.Text.Trim().ToLower();
                filters = text.Split(' ');
            }

            lbVehicleList.BeginUpdate();

            lbVehicleList.Items.Clear();

            foreach (var item in Data.Values)
            {
                string value = item.Key;

                if (filters != null)
                {
                    //Filtering
                    bool isIn = true;

                    for (int i = 0; i < filters.Length && isIn; i++)
                    {
                        isIn = value.ToLower().Contains(filters[i]);
                    }

                    if (isIn)
                    {
                        lbVehicleList.Items.Add(value);
                    }
                }
                else
                {
                    //No filtering
                    lbVehicleList.Items.Add(value);
                }
            }

            lbVehicleList.EndUpdate();

        }

        private void btnRefreshVehData_Click(object sender, EventArgs e)
        {
            if (tbVehicleSearch.Text == "")
                fillDatabaseList();
            else
                tbVehicleSearch.Text = ""; // Will cause the list to be updated

        }

        private void tbVehicleSearch_TextChanged(object sender, EventArgs e)
        {
            fillDatabaseList();
        }

        private void btnSaveVehData_Click(object sender, EventArgs e)
        {
            Data.Save();
        }

        private void btnCreateVeh_Click(object sender, EventArgs e)
        {
            new CreateVehicle(this);
        }

        private void btnEditVeh_Click(object sender, EventArgs e)
        {
            string login = lbVehicleList.SelectedItem.ToString();

            if (!Data.Values.ContainsKey(login))
            {
                fillDatabaseList();
                return;
            }

            Vehicle veh = Data.Values[login];

            new EditVehicle(this, veh);
        }

        private void btnRemoveVeh_Click(object sender, EventArgs e)
        {
            string login = lbVehicleList.SelectedItem.ToString();

            DialogResult result = MessageBox.Show("Do you want to delete " + login +"? This can not be undone!", "Are you sure? This can not be undone!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (result == DialogResult.OK)
            {
                Data.Values.Remove(login);
            }

            fillDatabaseList();
        }

        private void chkAutoStart_CheckedChanged(object sender, EventArgs e)
        {
            SaveSettings();
        }

        public void SaveSettings()
        {
            FileStream stream = new FileStream(SETTINGFILE, FileMode.Create);
            XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8);
            writer.WriteStartDocument();

            writer.WriteStartElement("Settings");

            writer.WriteStartElement("Port");
            int port = 0;
            if (!Int32.TryParse(tbPort.Text, out port))
                port = 5128;

            writer.WriteString(port + "");
            writer.WriteEndElement();

            writer.WriteStartElement("AutoStart");
            if (chkAutoStart.Checked)
                writer.WriteString("1");
            else
                writer.WriteString("0");
            writer.WriteEndElement();

            writer.WriteEndElement();

            writer.WriteEndDocument();

            writer.Flush();
            writer.Close();
            stream.Close();
        }

        private void LoadSettings()
        {
            XmlTextReader reader = null;

            try
            {
                reader = new XmlTextReader(SETTINGFILE);

                reader.ReadStartElement("Settings");

                reader.ReadStartElement("Port");
                int port = 0;
                if (!Int32.TryParse(reader.ReadString(), out port))
                    port = 5128;

                tbPort.Text = port + "";
                reader.ReadEndElement();

                reader.ReadStartElement("AutoStart");
                chkAutoStart.CheckedChanged -= new EventHandler(chkAutoStart_CheckedChanged);
                chkAutoStart.Checked = reader.ReadString() == "1";
                chkAutoStart.CheckedChanged += new EventHandler(chkAutoStart_CheckedChanged);
                reader.ReadEndElement();

                reader.Close();
            }
            catch (Exception)
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                }
                catch (Exception)
                {
                }
                finally
                {
                    tbPort.Text = "5128";

                    chkAutoStart.CheckedChanged -= new EventHandler(chkAutoStart_CheckedChanged);
                    chkAutoStart.Checked = false;
                    chkAutoStart.CheckedChanged += new EventHandler(chkAutoStart_CheckedChanged);
                }
            }
        }
    }
}
