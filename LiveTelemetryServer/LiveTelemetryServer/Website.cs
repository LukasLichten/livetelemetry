﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LiveTelemetryServer
{
    public class Website
    {
        private MainForm Main;
        private HttpListener Listener;

        internal Website(MainForm main, HttpListener listener)
        {
            this.Main = main;
            this.Listener = listener;
        }

        internal void handleRequest(HttpListenerContext context)
        {
            //TODO: Implement


            HttpListenerResponse resp = context.Response;
            resp.StatusCode = 404;
            resp.StatusDescription = "Website not implented yet";

            resp.Close();
        }
    }
}
