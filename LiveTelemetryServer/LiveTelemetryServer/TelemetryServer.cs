﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LiveTelemetryServer
{
    class TelemetryServer
    {
        private MainForm Main;
        private Website Website;

        private HttpListener Listner = new HttpListener();
        private int port;

        private bool stopping = false;

        public int NumberOfRequests { get; internal set; }

        internal TelemetryServer(int port, MainForm main)
        {
            this.Main = main;

            this.port = port;
            Listner.Prefixes.Add(String.Format(@"http://+:{0}/", ""+port));
            Listner.AuthenticationSchemes = AuthenticationSchemes.Basic;
            Listner.Start();


            this.Website = new Website(Main, Listner);

            Thread thread = new Thread(HandleRequests);
            thread.Name = "HttpHandler";
            thread.Start();
        }

        internal void Stop()
        {
            stopping = true;

            Thread.Sleep(200);

            if (Listner.IsListening)
            {
                HttpClient client = new HttpClient();
                client.GetAsync(String.Format("http://127.0.0.1:{0}/close", "" + port));

                Thread.Sleep(1000);

                client.Dispose();

                //Worst case we finally kill this thing
                if (Listner.IsListening)
                    Listner.Abort();
            }
        }

        private void HandleRequests()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            while (!stopping)
            {
                try
                {
                    NumberOfRequests++;

                    var context = Listner.BeginGetContext(new AsyncCallback(CallbackHandler), null);
                    context.AsyncWaitHandle.WaitOne();
                }
                catch (Exception)
                {
                    
                }
            }

            Thread.Sleep(5000);

            Listner.Close();
        }

        private void CallbackHandler(IAsyncResult ar)
        {
            HttpListenerContext context = Listner.EndGetContext(ar);

            Uri url = context.Request.Url;

            try
            {
                string requestIpAddress = context.Request.RemoteEndPoint.ToString();

                if (url.AbsolutePath == "/close")
                {
                    //This is called to stop the loop, so nothing happens here
                    return;
                }
                else if (url.AbsolutePath == "/test")
                {
                    //A Client opening a new connection
                    Vehicle veh = GetVehicle(url.Query);

                    if (veh == null)
                    {
                        //Incorrect accessdata
                        HttpListenerResponse resp = context.Response;
                        resp.StatusCode = 403;
                        resp.StatusDescription = "Login and or Password incorrect";

                        resp.Close();
                    }
                    else
                    {
                        //Everything fine
                        HttpListenerResponse resp = context.Response;

                        Stream output = resp.OutputStream;
                        StreamWriter writer = new StreamWriter(output);

                        writer.WriteLine("Connected!");

                        writer.Flush();
                        writer.Close();

                        output.Close();
                        resp.Close();
                    }
                }
                else if (url.AbsolutePath == "/send")
                {
                    //Data incoming
                    Vehicle veh = GetVehicle(url.Query);

                    if (veh == null)
                    {
                        //Incorrect accessdata
                        HttpListenerResponse resp = context.Response;
                        resp.StatusCode = 403;
                        resp.StatusDescription = "Login and or Password incorrect";

                        resp.Close();
                    }
                    else
                    {
                        HttpListenerResponse resp = context.Response;

                        //incoming data
                        Stream input = context.Request.InputStream;
                        StreamReader reader = new StreamReader(input);

                        string[] keyPairs = reader.ReadToEnd().Split('&');

                        Dictionary<string, string> data = new Dictionary<string, string>();

                        foreach (string item in keyPairs)
                        {
                            string edit = item;
                            edit = edit.Replace("+", " ");
                            edit = edit.Replace("%3A", ":");
                            edit = edit.Replace("%23", "#");


                            string[] pair = edit.Split('=');
                            data.Add(pair[0], pair[1]);
                        }

                        reader.Close();
                        input.Close();

                        try
                        {
                            veh.updateData(data, requestIpAddress);
                        }
                        catch (AccessViolationException)
                        {
                            resp.StatusCode = 423;
                            resp.StatusDescription = "This account is currently being written to";

                            resp.Close();
                            return;
                        }

                        //Everything fine

                        Stream output = resp.OutputStream;

                        if (veh.Data != null)
                        {
                            Dictionary<string, string> compactData = new Dictionary<string, string>();


                            if (veh.Data.ContainsKey("DriverStintTime"))
                                compactData.Add("DriverStintTime", veh.Data["DriverStintTime"]);

                            if (veh.Data.ContainsKey("DriverTotalTime"))
                                compactData.Add("DriverTotalTime", veh.Data["DriverTotalTime"]);


                            HttpContent content = new FormUrlEncodedContent(compactData);
                            content.CopyToAsync(output, null).Wait();
                        }

                        output.Close();
                        resp.Close();
                    }
                }
                else if (url.AbsolutePath == "/receive")
                {
                    //Data outgoing
                    Vehicle veh = GetVehicle(url.Query);

                    if (veh == null)
                    {
                        //Incorrect accessdata
                        HttpListenerResponse resp = context.Response;
                        resp.StatusCode = 403;
                        resp.StatusDescription = "Login and or Password incorrect";

                        resp.Close();
                    }
                    else
                    {
                        //Everything fine
                        HttpListenerResponse resp = context.Response;

                        Stream output = resp.OutputStream;

                        if (veh.Data != null)
                        {
                            HttpContent content = new FormUrlEncodedContent(veh.Data);
                            content.CopyToAsync(output, null).Wait();
                        }

                        output.Close();
                        resp.Close();
                    }
                }
                else if (url.AbsolutePath == "/ending")
                {
                    //Data incoming
                    Vehicle veh = GetVehicle(url.Query);

                    if (veh == null)
                    {
                        //Incorrect accessdata
                        HttpListenerResponse resp = context.Response;
                        resp.StatusCode = 403;
                        resp.StatusDescription = "Login and or Password incorrect";

                        resp.Close();
                    }
                    else
                    {
                        veh.closeDriver(requestIpAddress);
                    }
                }
                else if (url.AbsolutePath.Contains("/web"))
                {
                    Website.handleRequest(context);
                }
                else
                {
                    HttpListenerResponse resp = context.Response;
                    resp.StatusCode = 405;
                    resp.StatusDescription = "This Method is not available";

                    resp.Close();
                }

                
            }
            catch (FormatException)
            {
                try
                {
                    HttpListenerResponse resp = context.Response;
                    resp.StatusCode = 400;
                    resp.StatusDescription = "The querry was badly formated";

                    resp.Close();
                }
                catch
                {
                    return; //Abandon all hope
                }
                
            }
            catch (Exception)
            {
                try
                {
                    HttpListenerResponse resp = context.Response;
                    resp.StatusCode = 500;
                    resp.StatusDescription = "Server encountered an error";

                    resp.Close();
                }
                catch
                {
                    return; //Abandon all hope
                }
            }
        }

        private Vehicle GetVehicle(string querry)
        {
            if (!querry.Contains("?Login=") || !querry.Contains("&Password="))
                throw new FormatException("The request querry is misformated");


            string login = querry.Substring("?Login=".Length, querry.IndexOf("&Password=") - "?Login=".Length);
            string password = querry.Substring(querry.IndexOf("&Password=") + "&Password=".Length);
            
            return Main.Data.GetVehicle(login, password);
        }
    }
}
